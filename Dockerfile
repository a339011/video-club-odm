FROM node
LABEL Luis Raúl Chacon Muñoz
WORKDIR /app
COPY . .
ENV HOME video-club
RUN npm install
EXPOSE 3000
CMD npm start


