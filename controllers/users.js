
const express = require('express');
const bcrypt = require('bcrypt');
const User = require('../models/user');

function list(req, res, next) {
    res.send('respond with list');
}

function index(req, res, next) {
    const id = req.params.id;
    res.send(`respond with index >>${id}`);
}


async function create(req, res, next) {
    const name = req.body.name;
    const lastName = req.body.lastName;
    const email = req.body.email;
    const password = req.body.password;

    const salt = await bcrypt.genSalt(10);

    const passwordHash = await bcrypt.hash(password, salt);

    let user = new User({
        name: name,
        lastName: lastName,
        email : email,
        password : passwordHash,
        salt : salt,
    });

    user.save().then(obj => res.status(200).json({
        message: "Usuario creado correctamente",
        obj: obj
    })).catch(ex => res.status(500).json({
        message: "No se pudo almacenar el usuario",
        obj: ex
    }));
}

function replace(req, res, next) {
    res.send('respond with replace');
}

function update(req, res, next) {
    res.send('respond with update');
}

function destroy(req, res, next) {
    res.send('respond with destroy');
}


module.exports = { list, index, create, replace, update, destroy };